package message

import (
	"context"
	"log"

	"api-server/internal/cloud"
)

func Message(client cloud.MessageClient, body string) {
	ctx := context.Background()

	queURL := createQueue(ctx, client)
	send(ctx, client, queURL, body)
}

func createQueue(ctx context.Context, client cloud.MessageClient) string {
	url, err := client.CreateQueue(ctx, "notification-queue")
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("create queue:", url)

	return url
}

func send(ctx context.Context, client cloud.MessageClient, queueURL string, body string) {
	id, err := client.Send(ctx, &cloud.SendRequest{
		QueueURL: queueURL,
		Body:     body,
		Attributes: []cloud.Attribute{
			{
				Key:   "Title",
				Value: "SQS send message",
				Type:  "String",
			},
		},
	})
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("send: message ID:", id)
}
