package cloud

import (
	"context"
)

type SendRequest struct {
	QueueURL   string
	Body       string
	Attributes []Attribute
}

type Attribute struct {
	Key   string
	Value string
	Type  string
}

type MessageClient interface {
	CreateQueue(ctx context.Context, queueName string) (string, error)
	Send(ctx context.Context, req *SendRequest) (string, error)
}
