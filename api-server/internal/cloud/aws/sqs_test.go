package aws_test

import (
	"api-server/internal/cloud/aws"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestNewSQS(t *testing.T) {
	sess, err := aws.New(aws.Config{
		Address: "http://localhost:9324",
		Region:  "eu-west-1",
		Profile: "localstack",
		ID:      "test",
		Secret:  "test",
	})

	require.NoError(t, err)

	sqs := aws.NewSQS(sess, time.Second*5)

	require.NotNil(t, sqs)
	require.IsType(t, aws.SQS{}, sqs)
}
