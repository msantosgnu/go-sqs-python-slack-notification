package aws_test

import (
	"api-server/internal/cloud/aws"
	"testing"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/stretchr/testify/require"
)

func TestAwsNew(t *testing.T) {
	sess, err := aws.New(aws.Config{
		Address: "http://localhost:9324",
		Region:  "eu-west-1",
		Profile: "localstack",
		ID:      "test",
		Secret:  "test",
	})
	require.NoError(t, err)
	require.NotNil(t, sess)
	require.IsType(t, &session.Session{}, sess)
	require.Equal(t, *sess.Config.Endpoint, "http://localhost:9324")
}
