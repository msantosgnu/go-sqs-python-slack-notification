package main

import (
	"api-server/internal/cloud/aws"
	"api-server/internal/message"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func main() {

	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/receiver", sendMessage).Methods("POST")

	fmt.Println("api is on :3000")
	log.Fatal(http.ListenAndServe(":3000", muxRouter))
}

type Notification struct {
	Origin  string `json:"origin"`
	Message string `json:"message"`
	Channel string `json:"channel"`
}

type ResponseSuccess struct {
	Status       string       `json:"status"`
	Message      string       `json:"message,omitempty"`
	Notification Notification `json:"notification"`
}

func sendMessage(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	var notification Notification

	err := json.NewDecoder(r.Body).Decode(&notification)

	if err != nil {
		fmt.Println(err.Error())
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	msg, err := json.Marshal(notification)

	if err != nil {
		fmt.Println(err.Error())
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	// Create a session instance.
	sess, err := aws.New(aws.Config{
		Address: "http://sqs-server:9324",
		Region:  "eu-west-1",
		Profile: "localstack",
		ID:      "test",
		Secret:  "test",
	})

	if err != nil {
		log.Fatalln(err)
	}

	message.Message(aws.NewSQS(sess, time.Second*5), string(msg))

	json.NewEncoder(w).Encode(ResponseSuccess{
		Status:       "success",
		Notification: notification,
	})
}
