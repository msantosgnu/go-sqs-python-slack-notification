# go-sqs-python-slack-notification

An application that receives JSON events via the Golang API and sends them to an AWS SQS queue, consumed by a Python worker, the messages are posted to Slack channels.

## Getting started

### Requirements

Before running this app we will need:

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [Slack App](https://api.slack.com/authentication/basics)

### Running locally (docker-compose)

Inside the cloned repository folder:

1. Edit Dockerfile in worker-server and insert your slack app token in environment variable

```
ENV SLACK_TOKEN <YOUR-SLACK-TOKEN>
```


2. Build and run the app:

```
docker-compose up
```

That's it! After building and running the app your should have 3 container services available on your machine. Now you can start sending messages to the API.

### Testing it

For the local environment you can test sending a message on the queue according to the following steps.

- POST application/json: 

```
curl -d '{"origin":"origin-log", "message":"message-log", "channel":"you-slack-channel"}' -H "Content-Type: application/json" -X POST http://localhost:3000/receiver
```

You should see the worker docker service logs changing and a new message on the queue named "notification-queue" if you access the panel on: http://localhost:9325

You can also see logs from the worker that consumes the message queue and sends them to the Slack channel.

- Launch a bash terminal within the worker container with:
```
docker exec -it <docker-worker-id> sh
```

- Read worker output log file
```
tail -f /var/log/cron.log
```