import os
import sys
import json
import logging
import logging.config

from src.sqsconsumer import SqsConsumer
from src.slack import SlackPublisher


logger = logging.getLogger('worker')
dict_conf = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': "%(asctime)s %(levelname)s %(message)s",
            'datefmt': "%Y-%m-%d+%H:%M:%S",
        },
    },
    'handlers': {
        'default': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
            'stream': sys.stderr,
        },
        'rotating_to_file': {
            'level': 'DEBUG',
            'class': "logging.handlers.RotatingFileHandler",
            'formatter': 'standard',
            "filename": "worker-server.log",
            "maxBytes": 2000,
            "backupCount": 3,
        },
    },
    'loggers': {
        '': {
            'handlers': ['default', 'rotating_to_file'],
            'level': 'INFO',
            'propagate': True
        }
    }
}

logging.config.dictConfig(dict_conf)

region_name = os.getenv('SQS_REGION', 'us-west-1')
aws_access_key_id = os.getenv('AWS_ACCESS_KEY_ID', 'test')
aws_secret_access_key = os.getenv('AWS_SECRET_ACCESS_KEY', 'test')
endpoint_url = os.getenv('SQS_ENDPOINT_URL', 'http://127.0.0.1:9324')
queue = os.getenv('SQS_QUEUE', 'notification-queue')

slack_token = os.getenv('SLACK_TOKEN', '')

try:
    logger.info(f'WORKER START')

    sqs = SqsConsumer(
        region_name,
        aws_access_key_id,
        aws_secret_access_key,
        endpoint_url,
        queue
    )

    messages = sqs.return_all()

    slack = SlackPublisher(token=slack_token)

    for message in messages:
        slack.post_message(
            channel=message['channel'],
            origin=message['origin'],
            message=message['message']
        )
        logger.info(f'MESSAGE: {message}')

except Exception as e:
    logger.error(e)
finally:
    logger.info(f'WORKER FINISH')
