from slack import WebClient


class SlackPublisher():

    def __init__(self, token=''):
        self.client = WebClient(token=token)
    
    def post_message(self, channel, origin, message):
        self.client.chat_postMessage(**self.get_payload(channel, origin, message))
    
    def get_payload(self, channel, origin, message):
        return {
            "channel": channel,
            "blocks": self.get_message_blocks(origin, message)
        }

    def get_message_blocks(self, origin, message):
        return [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"*Notification {origin}*"
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": f"{message}"
                }
            }
        ]
