import boto3
import json


class SqsConsumer():

    def __init__(self, region_name='', aws_access_key_id='', aws_secret_access_key='', endpoint_url='', queue_name=''):
        self.sqs = boto3.resource('sqs', region_name=region_name, aws_access_key_id=aws_access_key_id, aws_secret_access_key=aws_secret_access_key, endpoint_url=endpoint_url)
        self.queue_name = queue_name
    
    def __get_queue_by_name(self):
        return self.sqs.get_queue_by_name(QueueName=self.queue_name)
    
    def return_all(self):
        queue = self.__get_queue_by_name()
        messages = queue.receive_messages()
        result = []
        while len(messages) > 0:
            message = messages[0]
            json_returned = json.loads(message.body)
            result.append(json_returned)        
            message.delete()
            messages = queue.receive_messages()
        return result
