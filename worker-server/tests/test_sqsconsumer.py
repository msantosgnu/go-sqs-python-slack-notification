import unittest
import json

from unittest.mock import patch
from src.sqsconsumer import SqsConsumer


class MockMessage():
    def __init__(self, queue, body):
        self.queue = queue
        self.body = body

    def delete(self):
        self.queue.messages.remove(self)


class MockQueue():
    def __init__(self):
        msg1 = MockMessage(self, json.dumps({'origin': 'syslog1', 'message': 'message test1', 'channel': 'general'}))
        msg2 = MockMessage(self, json.dumps({'origin': 'syslog2', 'message': 'message test2', 'channel': 'general'}))
        self.messages = [msg1, msg2]
    
    def receive_messages(self):
        return self.messages


class MockResource():
    def __init__(self):
        pass

    def get_queue_by_name(self, QueueName):
        return MockQueue()


def mock_boto_resource(sqs, region_name, endpoint_url, aws_access_key_id, aws_secret_access_key):
    return MockResource()


class TestSqsConsumer(unittest.TestCase):

    @patch('boto3.resource', side_effect=mock_boto_resource)
    def test_should_return_all_messages(self, mock_boto_resource):

        sqs = SqsConsumer(
            endpoint_url='http://sqs-test:9324',
            queue_name='queue-test'
        )
        result = sqs.return_all()

        self.assertEqual(len(result), 2)
        self.assertDictEqual(
            result[0],
            {'origin': 'syslog1', 'message': 'message test1', 'channel': 'general'}
        )
        self.assertDictEqual(
            result[1],
            {'origin': 'syslog2', 'message': 'message test2', 'channel': 'general'}
        )


if __name__ == '__main__':
    unittest.main()
