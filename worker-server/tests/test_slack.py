import unittest

from src.slack import SlackPublisher


class TestSlackPublisher(unittest.TestCase):

    def test_should_return_a_payload_block(self):

        slack = SlackPublisher(
            token='token-test'
        )
        
        block = slack.get_payload(
            channel='text-channel',
            origin='text-origin',
            message='text-message'
        )

        self.assertIsInstance(block, dict)

        expected = {
            "channel": "text-channel",
            "blocks": slack.get_message_blocks("text-origin", "text-message")
        }

        self.assertDictEqual(block, expected)

    def test_should_return_a_message_block(self):

        slack = SlackPublisher(
            token='token-test'
        )
        
        blocks = slack.get_message_blocks(
            origin='text-origin',
            message='text-message'
        )

        self.assertIsInstance(blocks, list)

        expected = [
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': '*Notification text-origin*'
                }
            },
            {
                'type': 'section',
                'text': {
                    'type': 'mrkdwn',
                    'text': 'text-message'
                }
            }
        ]

        self.assertListEqual(blocks, expected)


if __name__ == '__main__':
    unittest.main()
